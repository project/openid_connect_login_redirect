CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------
This module creates a single entry point to login the user against the
configured IDP. If the user isn't logged in, the IDP will show the login
form, otherwise the user will be authenticated in Drupal and redirected
to any accessible page.

This extension is useful for example to access an intranet through a single
URL. It is also useful in an ecosystem where external access to well-known
content is sought, since once the user is authenticated, he/she can be
automatically redirected to that content.

The exposed entry point is the following:
/openid-connect/{client_name}/redirect

client_name: it's the Plugin ID for the active openid_connect client
(like facebook/google/github/keycloak).

A full url example for keycloak would be:
/openid-connect/keycloak/redirect

[1] https://www.drupal.org/project/openid_connect_login_redirect
[2] https://www.drupal.org/project/openid_connect

REQUIREMENTS
------------
Only openid_connect is required and an openid-connect client to be configured.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. For further
information, see:
   https://www.drupal.org/docs/extending-drupal/installing-modules

CONFIGURATION
-------------
No special configuration is required.

MAINTAINERS
-----------
Current maintainers:
  * Nicolás Bottini Turín (nicobot) - https://www.drupal.org/u/nicobot

