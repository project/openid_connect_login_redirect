<?php

namespace Drupal\openid_connect_login_redirect\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\openid_connect\OpenIDConnectClaims;
use Drupal\openid_connect\OpenIDConnectSession;
use Drupal\openid_connect\Plugin\OpenIDConnectClientInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Routing\RedirectDestinationInterface;


/**
 * Redirect controller.
 *
 * @package Drupal\openid_connect_login_redirect\Controller
 */
class OpenIDConnectLoginRedirectController extends ControllerBase implements AccessInterface {

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\openid_connect\OpenIDConnectSession
   */
  protected $session;

  /**
   * The OpenID client plugin manager.
   *
   * @var \Drupal\openid_connect\Plugin\OpenIDConnectClientManager
   */
  protected $pluginManager;

  /**
   * The OpenID Connect claims.
   *
   * @var \Drupal\openid_connect\OpenIDConnectClaims
   */
  protected $claims;

  /**
   * The request stack used to access request globals.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The constructor.
   *
   * @param \Drupal\openid_connect\OpenIDConnectSession $session
   *   The OpenID Connect session service.
   * @param \Drupal\openid_connect\Plugin\OpenIDConnectClientManager $plugin_manager
   *   The OpenID client plugin manager.
   * @param \Drupal\openid_connect\OpenIDConnectClaims $claims
   *   The OpenID state token service.
   * @param RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(
    OpenIDConnectSession $session,
    OpenIDConnectClientManager $plugin_manager,
    OpenIDConnectClaims $claims,
    RedirectDestinationInterface $redirect_destination,
    LoggerChannelFactoryInterface $logger_factory,
    AccountInterface $account
  ) {
    $this->session = $session;
    $this->claims = $claims;
    $this->pluginManager = $plugin_manager;
    $this->redirectDestination = $redirect_destination;
    $this->loggerFactory = $logger_factory;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): OpenIDConnectLoginRedirectController {
    return new static(
      $container->get('openid_connect.session'),
      $container->get('plugin.manager.openid_connect_client'),
      $container->get('openid_connect.claims'),
      $container->get('redirect.destination'),
      $container->get('logger.factory'),
      $container->get('current_user')
    );
  }

  /**
   * Redirects user to provider in order to validate or initiate a session.
   *
   * Once the session is active, the user will be redirected to the specified
   * destination from the GET parameter.
   *
   * @param string $client_name
   *   The client name.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Redirect response
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function loginRedirect(string $client_name): \Symfony\Component\HttpFoundation\Response {

    if ($this->account && $this->account->isAuthenticated()) {
      $url = Url::fromUri('internal:/');
      $redirect =  $url->mergeOptions(['query' => $this->redirectDestination->getAsArray()]);
      $generated_redirect = $redirect->toString(TRUE);

      $response =  new TrustedRedirectResponse($generated_redirect->getGeneratedUrl());
      $response->addCacheableDependency($generated_redirect);
      return  $response;
    }

    $configuration = $this->config('openid_connect.settings.' . $client_name)
      ->get('settings');

    $client = null;
    if ($configuration) {
      /** @var \Drupal\openid_connect\Plugin\OpenIDConnectClientInterface $client */
      $client = $this->pluginManager->createInstance(
        $client_name,
        $configuration
      );
    }

    if (!($client instanceof OpenIDConnectClientInterface)) {
      $params = [
        '@client_name' => $client_name,
      ];
      $message = 'Redirect failed: Unknown or not configured @client_name.';
      $this->loggerFactory->get('openid_connect_login_redirect')->error($message, $params);
      $this->messenger()->addError($this->t($message, $params));

      // In case we don't have an error, but the client could not be loaded or
      // there is no state token specified, the URI is probably being visited
      // outside of the login flow.
      throw new NotFoundHttpException();
    }

    $this->saveDestination();

    $scopes = $this->claims->getScopes($client);
    $_SESSION['openid_connect_op'] = 'login';

    return $client->authorize($scopes);
  }

  /**
   * Stores the redirection indicated by the user.
   *
   * After the user gets authorized, it will be redirected to the indicated path.
   * This should be indicated in the parameter "destination".
   *
   * @return void
   */
  protected function saveDestination(): void {

    $destination_url = $this->redirectDestination->get();

    // The destination could contain query parameters. Ensure that they are
    // preserved.
    $parsedURL = UrlHelper::parse($destination_url);

    $destination_info = [
      $parsedURL['path'],
      [
        'query' => UrlHelper::buildQuery($parsedURL['query']),
      ],
    ];

    $_SESSION['openid_connect_destination'] = $destination_info;
  }
}
